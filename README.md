# WAAI

WAAI is a Web Assembly Actor Interface, intended to support WASM modules that follow the Actor Model. It is motivated by the WASI system interface. This repo contains the WAAI spec and implementations for various WASM VMs.